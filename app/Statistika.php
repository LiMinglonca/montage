<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistika extends Model
{


    public function check_repeats($orders){

        $br=0;
        foreach($orders as $order){
            if($this->order == $order->order){
                $br++;
                $order->br = $br;
            }
        }

        return $this->br;
    }

    public function check_status($orders){
        $flag = false;

        foreach($orders as $order){
            if($this->order == $order->order){
            if($order->real_end){
                $flag = true;
            }
            else{
                $flag = false;
                break;
            }
        }
    }
        return $flag;
    }

    protected $fillable = ['vrijemeRada'];
}
