<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'order_number',
        'status',
        'planned_start',
        'real_start',
        'planned_end',
        'real_end',
        'priority',
        'operation_number',
        'workstation_number',
        'worker_number',
    ];


    public function check_repeats($orders){

        $br=0;
        foreach($orders as $order){
            if($this->order_number == $order->order_number){
                $br++;
                $order->br = $br;
            }
        }

        return $this->br;
    }

    public function check_status($orders){
        $flag = false;

        foreach($orders as $order){
            if($this->order_number == $order->order_number){
            if($order->status == true){
                $flag = true;
            }
            else{
                $flag = false;
                break;
            }
        }
    }
        return $flag;
    }
}
