<?php

namespace App\Http\Controllers;

use App\Order;
use App\Statistika;
use Illuminate\Http\Request;
use App\Gantogram;
use Response;

class StatistikaController extends Controller
{

    public function dowload(Request $request){

        $sorders = Statistika::where('order',$request->order)->get();

        foreach($sorders as $sorder){
            $sorder->planned_start=date('H:i d-m-y',$sorder->planned_start);
            $sorder->real_start=date('H:i d-m-y',$sorder->real_start);
            $sorder->planned_end=date('H:i d-m-y',$sorder->planned_end);
            $sorder->real_end=date('H:i d-m-y',$sorder->real_end);
            $sorder->real_end=date('H:i d-m-y ',strtotime($sorder->real_end));
        }
      #  return $sorders;

        $filename = "Nalog ".$request->order.".csv";
        $handle = fopen($filename, 'w');


        fputcsv($handle, array('order', 'operacija', 'radnik', 'vrijeme rada','planirani pocetak','stvarni pocetak','planirani kraj','stvarni kraj'));

        foreach($sorders as $row) {
            fputcsv($handle, array($row['order'], $row['opracija'], $row['worker'], $row['vrijemeRada'],$row['planned_start'],$row['real_start'],$row['planned_end'], $row['real_end']));
        }

        fclose($handle);

        $headers = array(
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=galleries.csv',
            'Expires'             => '0',   'Pragma'              => 'public'

        );

        return Response::download($filename, 'Nalog '.$request->order.'.csv', $headers);
    }







    public function reset(Request $request){

      # return $request;
       $order=Order::find($request->order_id);
       $statistika=Statistika::where('order_id',$request->order_id)->first();

        $statistika->real_start = null;
        $order->real_start = null;
        $statistika->save();
        $order->save();



        return redirect()->route('radnikView.index');
    }

    public function index()
    {
        $orders = Statistika::all();

       # return $orders;
        return view('backend.statistika.show', compact('orders'));
    }




    public function create()
    {
        //
    }


    public function store(Request $request)
    {

       # return $request->time;

        $date = time();

        $order=Order::find($request->order_id);

        $order->status=true;
        $order->real_end= $date;
        $order->worker_number = $request->worker;
        $order->save();

        $gant = Gantogram::where('order_id',$order->id)->first();
        $gant->order_status = true;
        $gant->save();


        $statistika = Statistika::where('order_id',$request->order_id)->first();
        $statistika->worker = $request->worker;
        $statistika->real_end = $date;
        $statistika->vrijemeRada=$request->time;
        $statistika->worker=$request->worker;
        $statistika->save();


        return redirect()->route('radnikView.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Statistika  $statistika
     * @return \Illuminate\Http\Response
     */
    public function show(Statistika $statistika)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Statistika  $statistika
     * @return \Illuminate\Http\Response
     */
    public function edit(Statistika $statistika)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Statistika  $statistika
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Statistika $statistika)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Statistika  $statistika
     * @return \Illuminate\Http\Response
     */
    public function destroy(Statistika $statistika)
    {
        //
    }
}
