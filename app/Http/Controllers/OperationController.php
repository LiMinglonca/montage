<?php

namespace App\Http\Controllers;

use App\Doc;
use App\Operation;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class OperationController extends Controller
{
    public function updateVideo(Request $request)
    {
        $vPath = public_path('Videos');
        $video=Video::find($request->video_id);
        $operation=Operation::findOrFail($video->operation_id);
        File::delete($video->path);

        $videoPath = $request->file('video');
        $video->name = $videoPath->hashName();
        $video->description = $operation->description;
        $video->path = $videoPath->move($vPath, $video->name);
        $video->operation_id = $operation->id;
        $video->user_name_of_video = $videoPath->getClientOriginalName();
        $video->save();



        session()->flash('sucess', 'Uspješnio izmjenjen video');
        return redirect()->route('operation.edit', $operation->id);

    }
    public function deleteVideo($id)
    {

        $video = Video::find($id);
        $operation = Operation::where('id', $video->operation_id)->first();
        File::delete($video->path);
        $docs = Doc::where('video_id', $video->id)->get();

        foreach ($docs as $doc) {
            File::delete($doc->path);
            $doc->delete();
        }
        $video->delete();

        return redirect()->route('operation.edit', $operation->id)->with('success', 'Uspješno izbrisan video');
    }
    public function storeDoc(Request $request)
    {
        $dPath = public_path('Docs');
        $video = Video::findOrFail($request->video);
        $operation = Operation::findOrFail($video->operation_id);

        foreach ($request->file('doc') as $file) {

            $doc = new Doc();
            $docPath = $file;
            $doc->name = $docPath->hashName();
            $doc->description = $operation->description;
            $doc->path = $docPath->move($dPath, $doc->name);
            $doc->video_id = $request->video;
            $doc->user_name_of_doc = $docPath->getClientOriginalName();
            $doc->save();
        }

        return redirect()->route('operation.edit', $operation);

    }
    public function deleteDoc($id)
    {
        Doc::destroy($id);
    }

    /**
     * sore video in db
     *
     * @return \Illuminate\Http\Response
     */
    public function storeVideo(Request $request)
    {

        $i = 0;
        $j = 0;
        $vPath = public_path('Videos');
        $dPath = public_path('Docs');
        $operation = Operation::find($request->operation_id);

        while ($request->file('video' . $i)) {
            $video = new Video();
            $videoPath = $request->file('video' . $i);
            $video->name = $videoPath->hashName();
            $video->description = $operation->description;
            $video->path = $videoPath->move($vPath, $video->name);
            $video->operation_id = $operation->id;
            $video->user_name_of_video = $videoPath->getClientOriginalName();
            $video->save();

            foreach ($request->file('doc' . $i) as $file) {

                $doc = new Doc();
                $docPath = $file;
                $doc->name = $docPath->hashName();
                $doc->description = $operation->description;
                $doc->path = $docPath->move($dPath, $doc->name);
                $doc->video_id = $video->id;
                $doc->user_name_of_doc = $docPath->getClientOriginalName();
                $doc->save();
                $j++;
            }

            $i++;

        }
        return redirect()->route('operation.edit', $operation);
    }

    /**
     * Display new video from.
     *
     * @return \Illuminate\Http\Response
     */
    public function addVideoForm($id)
    {
        $operation = Operation::findOrFail($id);

        return view('backend.operation.newVideo', compact('operation'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $operations = Operation::all();

        return view('backend.operation.show', compact('operations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.operation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $this->validate($request, [
        //     'operation_number' => 'unique:operations|required',
        //     'title'=>'required',
        //     'description'=>'required|max:255|min:4'

        // ]);

        $i = 0;
        $j = 0;
        $vPath = public_path('Videos');
        $dPath = public_path('Docs');

        $operation = new Operation();
        $operation->title = $request->title;
        $operation->description = $request->description;
        $operation->operation_number = $request->operation_number;
        $operation->save();

        if ($request->file('video' . $i) != null) {

            while ($request->file('video' . $i)) {
                $video = new Video();
                $videoPath = $request->file('video' . $i);
                $video->name = $videoPath->hashName();
                $video->description = $request->description;
                $video->path = $videoPath->move($vPath, $video->name);
                $video->operation_id = $operation->id;
                $video->user_name_of_video = $videoPath->getClientOriginalName();
                $video->save();

                foreach ($request->file('doc' . $i) as $file) {

                    $doc = new Doc();
                    $docPath = $file;
                    $doc->name = $docPath->hashName();
                    $doc->description = $request->description;
                    $doc->path = $docPath->move($dPath, $doc->name);
                    $doc->video_id = $video->id;
                    $doc->user_name_of_doc = $docPath->getClientOriginalName();
                    $doc->save();

                }

                $i++;
            }

            session()->flash('sucess', 'Uspješno dodana operacija ' . $operation->number);
            return redirect()->route('operation.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Operation  $operation
     * @return \Illuminate\Http\Response
     */
    public function show(Operation $operation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Operation  $operation
     * @return \Illuminate\Http\Response
     */
    public function edit(Operation $operation)
    {

        $videos = Video::where('operation_id', $operation->id)->get();

        $docs = [];

        foreach ($videos as $video) {
            $doc = Doc::where('video_id', $video->id)->get();
            array_push($docs, [$video->id => $doc]);
        }

        return view('backend.operation.edit', compact('operation', 'videos', 'docs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Operation  $operation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Operation $operation)
    {

        $operation->update($request->all());

        return redirect()->route('operation.edit', $operation);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Operation  $operation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Operation $operation)
    {
        $number = $operation->number;

        $videos = Video::where('operation_id', $operation->id)->get();
        foreach ($videos as $video) {
            File::delete($video->path);

            $docs = Doc::where('video_id', $video->id)->get();
            foreach ($docs as $doc) {
                File::delete($doc->path);
                $doc->delete();
            }
            $video->delete();
        }
        $operation->delete();

        session()->flash('sucess', 'Uspješno izbrisana operacija ' . $number);
        return redirect()->route('operation.index');
    }
}
