<?php

namespace App\Http\Controllers;
use App\Tmp;
use Session;
use App\Radnik;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function show(){
        return view('show');
    }

    public function prijava(){


        $rfid=Tmp::first();




        if(!isset($rfid)){

            return redirect('http://localhost/client-montage/home.php');
        }




        $radnik=Radnik::where('rfid',$rfid->rfid)->first();
        Tmp::get()->each->delete(); //brisi sve


        #return $radnik;

        if(!isset($radnik)){
            return redirect('http://localhost/client-montage/home.php');#### teba preimenovati u index.php
        }
        Session::put('radnik', $radnik);


        if($radnik->isAdmin){
            return redirect()->route('order.index');
        }
        else{
            return redirect()->route('radnikView.index');
        }

    }

    public function odjava(Request $request){


            Session::flush();
            return redirect('http://localhost/client-montage/home.php');

    }

}
