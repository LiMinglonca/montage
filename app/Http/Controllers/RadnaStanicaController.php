<?php

namespace App\Http\Controllers;

use App\RadnaStanica;
use Cookie;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;
use Validator;
use File;
use Response;

class RadnaStanicaController extends Controller
{

    public function ShowImportForm()
    {
        return view('backend.radnaStanica.import');
    }

    public function ImportCsvData(Request $request)
    {

        if ($request->file('dat') != null) {


            $file = $request->file('dat');

            // File Details
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();

            // Valid File Extensions
            $valid_extension = array("csv");

            // 2GB in Bytes
            $maxFileSize = 20971520;

            // Check file extension
            if (in_array(strtolower($extension), $valid_extension)) {

                // Check file size
                if ($fileSize <= $maxFileSize) {

                    // File upload location
                    $location ='storage\\tmp';

                    // Upload file
                    $file->move($location, $filename);

                    // Import CSV to Database
                    $filepath = public_path($location . "\\" . $filename);

                    // Reading file
                    $file = fopen($filepath, "r");



                 $collection = (new FastExcel)->import($filepath);
                 fclose($file);

                File::delete($filepath);





                    foreach ($collection as $radnaStanica) {
                        $validator = Validator::make($radnaStanica, ['broj' => 'bail|unique:radna_stanicas',
                                                                      'opis'=>'required|max:255|min:4']);

                        if ($validator->fails()) {

                            if(isset($radnaStanica['broj'])){

                            session()->flash('error', 'Radna stanica '.$radnaStanica['broj'].' već postoji, isptavite csv datoteku');
                            return redirect()->route('import.radnaStanica.form');
                            }
                            else{

                                session()->flash('error', 'Csv datoteka nije valjana');
                                return redirect()->route('import.radnaStanica.form');
                            }

                        } else {


                            try {

                                RadnaStanica::create($radnaStanica);
                            } catch (QueryException $ex) {

                                session()->flash('error', 'Struktura datoteke nije valjana ');
                                return redirect()->route('import.radnaStanica.form');

                            }

                        }

                    }
                    session()->flash('sucess', 'Uspješno !');
                    return redirect()->route('radnaStanica.index');

                }
            }

            session()->flash('error', 'ne valja ektenzija');
            return redirect()->route('import.radnaStanica.form');
        }

        session()->flash('error', 'Učitajte datoteku');
        return redirect()->route('import.radnaStanica.form');
    }

    public function setCookie(Request $request)
    {

        if (Cookie::get($request->radnaStanica_broj) == null) {

            $minutes = 99999999;
            $response = new Response();
            $cookie = cookie($request->radnaStanica_broj, $request->radnaStanica_id, $minutes);
            $radnaStanica = RadnaStanica::find($request->radnaStanica_id);
            $radnaStanica->setCookie();

            return redirect()->route('radnaStanica.edit', $request->radnaStanica_id)->withCookie($cookie);

        }
        return redirect()->route('radnaStanica.edit', $request->radnaStanica_id)->withCookie($cookie);

    }

    public function getCookie(Request $request)
    {
        return $request;
        $value = $request->cookie('setRadnaStanica1');
        echo $value;
    }

    public function index()
    {
        $radneStanice = radnaStanica::orderBy('cookie', 'asc')->get();
        return view('backend.radnaStanica.show', compact('radneStanice'));
    }

    public function create()
    {
        return view('backend.radnaStanica.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'broj' => 'bail|required|unique:radna_stanicas|max:255',
            'opis' => 'required|max:255',
        ]);

        RadnaStanica::create($request->all());

        session()->flash('sucess', 'Uspješno dodana radna stanica !');
        return redirect()->route('radnaStanica.index');

    }

    public function show(RadnaStanica $radnaStanica)
    {
        //
    }

    public function edit($id)
    {
        $radneStanice = RadnaStanica::all();
        $radnaStanica = RadnaStanica::find($id);
        return view('backend.radnaStanica.edit', compact('radnaStanica', 'radneStanice'));
    }

    public function update(Request $request, $id)
    {
        RadnaStanica::where('id', $id)->update($request->except(['_token', '_method']));
        $radneStanice = RadnaStanica::orderBy('broj', 'asc')->get();
        return view('backend.radnaStanica.show', compact('radneStanice'));
    }
    public function removeRS($id){
        $radnaStanica = RadnaStanica::find($id);
        $broj = $radnaStanica->broj;
        $radnaStanica->unsetCookie();

        $radneStanice = radnaStanica::orderBy('broj', 'asc')->get();

        return redirect()->route('radnaStanica.index')->cookie(cookie()->forget($broj));

    }
    public function destroy($id)
    {

        $radnaStanica = RadnaStanica::find($id);
        $broj = $radnaStanica->broj;
        $radnaStanica->delete();

        $radneStanice = radnaStanica::orderBy('broj', 'asc')->get();

        return redirect()->route('radnaStanica.index')->cookie(cookie()->forget($broj));

    }
}
