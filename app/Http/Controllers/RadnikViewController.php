<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use App\RadnikView;
use App\Video;
use App\Doc;
use App\Operation;
use App\RadnaStanica;
use Illuminate\Http\Request;
use File;
use App\Order;
use App\Statistika;
use Cookie;
use PhpParser\Node\Stmt\Static_;
use Session;


class RadnikViewController extends Controller
{


    public function showOperation(Request $request){


        $border=Order::find($request->order_id);
        $date = time();
        $border->real_start=$date;
        $border->save();

        $statistika =Statistika::where('order_id',$border->id)->first();

       # $statistika->order_id = $border->id;
        $statistika->real_start = $date;
        $statistika->save();



        $order=$request->order_number;
        $order_id = $border->id;

        Session::put('order', $order);
          $operacija=Operation::where('operation_number','=',$request->operation_number)->first();/// ovo se mjenja


          if($operacija){
              $videos=Video::where('operation_id',$operacija->id)->get();
              $radneStanice=RadnaStanica::all();
              $dokumenti=[];

            foreach($videos as $video){
              $dokument=Doc::where('video_id',$video->id)->get();
              array_push($dokumenti, [$video->id =>$dokument]);
              }
              return view('frontend.home',compact('operacija','videos','dokumenti','radneStanice','order','order_id'));
            }

            Session::put('Error','Operacija ne postoji');

           return redirect()->back();


      }


      public function index()
      {

      Session::forget('Error');
      // return Cookie::get('');
      $orders=Order::all();
      $radneStanice=RadnaStanica::all();
      $cookie='NULL';

        if(!$radneStanice->isEmpty()){
          foreach($radneStanice as $radnaStanica){
              if(Cookie::get($radnaStanica->broj)){
                  Session::put('workstation', $radnaStanica->broj);
              }
          }
        }


        #print_r($cookie);

        return view('frontend.orderList', compact('orders'));


    }











    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        $video=Video::find($id);
        $dokumenti=Dokument::find('video_id',$video->id);


        return $video;
    }


    public function edit(RadnikView $radnikView)
    {
        //
    }


    public function update(Request $request, RadnikView $radnikView)
    {
        //
    }

    public function destroy(RadnikView $radnikView)
    {
        //
    }
}
