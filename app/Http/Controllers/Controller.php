<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Rap2hpoutre\FastExcel\FastExcel;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
   
    //Storing csv file in storage
    public function storeInStorage($file, $path)
    {

        $filename = time() . '_' . $file->getClientOriginalName();

        if ($file->move($path, $filename)) {
            $path = $path . '\\' . $filename;
            return $path;
        }
        return false;

    }

    //Citanje datoteke iz storage-a
    public function readFile($path)
    {

        $file = fopen($path, "r");
        $fileContent = (new FastExcel)->import($path);
        fclose($file);

        return $fileContent;
    }

}
