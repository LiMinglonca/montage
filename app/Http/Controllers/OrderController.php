<?php

namespace App\Http\Controllers;

use App\Order;
use App\Gantogram;
use App\Statistika;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Validator;
use Illuminate\Support\Carbon;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $orders = Order::all();
        return view('backend.order.show', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.order.import');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $order_obj=$order;
        $orders = Order::where('order_number',$order->order_number)->orderby('operation_number','asc')->get();
        $statistika= Statistika::where('order',$order->order_number)->get();
        $order_number = $order->order_number;

        return view('backend.order.details', compact('orders','order_number','order_obj','statistika'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $orders = Order::where('order_number',$order->order_number)->get();
        $sorders= Statistika::where('order',$order->order_number)->get();
        $number = $order->number;

        foreach($orders as $order){
            $order_id = $order->id;
            File::delete($order->adress);
            $gant = Gantogram::where('order_id','=',$order_id)->first();
            if($gant){
            $gant->delete();
            }


            foreach($sorders as $sorder){
                if($sorder->real_end == null){
                    $sorder->delete();
                }
            }

            $order->delete();
        }

            session()->flash('sucess', 'Uspješno izbrisan nalog ' . $number);
            return redirect()->route('order.index');

    }

    /**
     *Spremanje importane datoteke u bazu---datoteka je spremljena u storage i validirana prethodno.
     *
     * @param  \$fileContent
     * @return \Illuminate\Http\Response
     */


    public function saveImportInDB($fileContent,$path)
    {


        foreach ($fileContent as $order) {

            try {

                $or = new Order();
                $or->order_number = $order['order_number'];
            #    $or->status = $order['status'];
                $or->planned_start =strtotime($order['planned_start']);
                $or->planned_end = strtotime($order['planned_end']);
                $or->priority = $order['priority'];
                $or->operation_number = $order['operation_number'];
                $or->workstation_number = $order['workstation_number'];
                $or->adress=$path;
                $or->save();

                $statistika = new Statistika();
                $statistika->order_id = $or->id;
                $statistika->order = $or->order_number;
                $statistika->opracija = $or->operation_number;
                $statistika->planned_start = $or->planned_start;
                $statistika->planned_end = $or->planned_end;

                $statistika->save();



                $gant = new Gantogram();

                $gant->order_id = $or->id;
                $gant->order_number = $or->order_number;
                $gant->workstation_number = $or->workstation_number;
                $gant->operation_number = $or->operation_number;
                $gant->real_start = $or->planned_start;
                $gant->real_end = $or->planned_end;

                $gant->save();



            } catch (QueryException $ex) { #!--------------------------------------------------------------------------------izmjeniti------------------------------------------------------!
            session()->flash('error', $ex->getMessage());
                return redirect()->route('order.create');

            }
        }

        session()->flash('sucess', 'Uspješno dodano');
        return redirect()->route('order.index');
    }

    /**
     *Importanje cs datoteke.
     *
     * @param  \$fileContent
     * @return \Illuminate\Http\Response
     */
    public function importCsvData(Request $request)
    {

        if ($request->file('orders') != null) {

            $file = $request->file('orders');

            return $this->validateFileExtension($file);

        } else {
            session()->flash('error', 'Učitajte datoteku !!!');
            return redirect()->route('order.create');
        }

    }
    /**
     *Provjerava tip datoteke.
     *
     * @param  \$fileContent
     * @return \Illuminate\Http\Response
     */
    private function validateFileExtension($file)
    {

        $extension = $file->getClientOriginalExtension();
        if ($extension == 'csv') {

            $path = public_path('storage\Orders'); //Direktorij gdje se spremaju .csv datoteka Naloga

            return $this->validateCsvFileContent($this->storeInStorage($file, $path));
        } else {
            session()->flash('error', 'Tip učitane datoteke nije podržan, učitajte .csv datoteku');
            return redirect()->route('order.create');
        }
    }

    /**
     *validiranje strukutre datoteke.
     *
     * @param  \$path putanje do datoteke
     * @return \Illuminate\Http\Response
     *
     */
    public function validateCsvFileContent($path)
    {

        if ($path) {
            $fileContent = $this->readFile($path);

            foreach ($fileContent as $order) {
                $validator = Validator::make($order, [
                  #  'planned_start' => 'required|date',
                   # 'planned_end' => 'required|date',
                    'priority' => 'required',
                    'operation_number' => 'required', #|exists:operations
                    'workstation_number' => 'required', #|exists:workstations',
                ]);

                if ($validator->fails()) {
                    File::delete($path);
                    session()->flash('error', $validator->errors()->first());
                    return redirect()->route('order.create');
                }

            }
            return $this->saveImportInDB($fileContent,$path);
        }

        return false;
    }

}
