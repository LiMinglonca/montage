<?php

namespace App\Http\Controllers;
use Storage;
use Validator;
use App\Radnik;
use Illuminate\Http\Request;
use Cookie;
use Illuminate\Database\QueryException;
use Illuminate\Http\Response;
use Rap2hpoutre\FastExcel\FastExcel;
use Illuminate\Support\Facades\File;


class RadnikController extends Controller
{


    public function izbrisi(Request $request){
        $radnik = Radnik::find($request->id);
        $radnik->isAdmin = false;
        $radnik->save();

        return redirect()->route('radnici.edit', $radnik->id);
    }
    public function postavi(Request $request){
        $radnik = Radnik::find($request->id);
        $radnik->isAdmin = true;
        $radnik->save();

        return redirect()->route('radnici.edit', $radnik->id);
    }



    public function index()
    {
        $radnici=Radnik::all();
        return view('backend.radnik.show',compact('radnici'));
    }



    public function ShowImportForm(){
        return view('backend.radnik.import');
    }

    public function ImportCsvData(Request $request)
    {

        if ($request->file('dat') != null) {


            $file = $request->file('dat');

            // File Details
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $tempPath = $file->getRealPath();
            $fileSize = $file->getSize();
            $mimeType = $file->getMimeType();

            // Valid File Extensions
            $valid_extension = array("csv");

            // 2GB in Bytes
            $maxFileSize = 20971520;

            // Check file extension
            if (in_array(strtolower($extension), $valid_extension)) {

                // Check file size
                if ($fileSize <= $maxFileSize) {

                    // File upload location
                    $location ='storage\\tmp';

                    // Upload file
                    $file->move($location, $filename);

                    // Import CSV to Database
                    $filepath = public_path($location . "\\" . $filename);

                    // Reading file
                    $file = fopen($filepath, "r");



                 $collection = (new FastExcel)->import($filepath);
                 fclose($file);

                File::delete($filepath);


                    foreach ($collection as $radnik) {
                        $validator = Validator::make($radnik, ['brojRadnika' => 'required|bail|unique:radniks',
                                                                'ime'=>'required',
                                                                'prezime'=>'required',
                                                                'rfid'=>'required|bail|unique:radniks']);

                        if ($validator->fails()) {


                            if(isset($radnik['brojRadnika'])){

                            session()->flash('error', 'Radnik sa ID-om '.$radnik['brojRadnika'].' već postoji, isptavite csv datoteku');
                            return redirect()->route('import.form');
                            }
                            else{

                                session()->flash('error', 'Csv datoteka nije valjana');
                                return redirect()->route('import.form');
                            }

                        } else {


                            try {

                                radnik::create($radnik);
                            } catch (QueryException $ex) {

                                session()->flash('error', 'Struktura datoteke nije valjana ');
                                return redirect()->route('import.form');

                            }

                        }

                    }
                    session()->flash('sucess', 'Uspješno !');
                    return redirect()->route('radnici.index');

                }
            }

            session()->flash('error', 'ne valja ektenzija');
            return redirect()->route('import.form');
        }

        session()->flash('error', 'Učitajte datoteku');
        return redirect()->route('import.form');
    }








    public function create()
    {
        return view('backend.radnik.create');
    }


    public function store(Request $request)
    {
        Radnik::create($request->all());

        return redirect()->route('radnici.index')->with('success', 'Uspješno dodan radnik');
    }


    public function show(Radnik $radnik)
    {
        //
    }


    public function edit($id)
    {
        $radnik=Radnik::find($id);
        return view('backend.radnik.edit', compact('radnik'));
    }


    public function update(Request $request, $id)
    {

       Radnik::where('id', $id)->update($request->except(['_token','_method']));
       $radnici=Radnik::orderBy('ime','asc')->get();
       return view('backend.radnik.show', compact('radnici'));


    }

    public function destroy($id)
    {
        Radnik::find($id)->delete();
        $radnici=Radnik::orderBy('ime','asc')->get();

        return redirect()->route('radnici.index');


    }
}
