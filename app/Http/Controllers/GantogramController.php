<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RadnaStanica;
use App\Order;
use App\Gantogram;

class GantogramController extends Controller
{
    public function index(){

       $data = Order::orderby('real_end','asc')->get();
       $RS = RadnaStanica::orderby('broj','asc')->get();
        return view('backend.gantogram.show', compact('data','RS'));
    }
}
