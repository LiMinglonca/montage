<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RadnaStanica extends Model
{

    public function NOTavailable(){
        $this->available=false;
        $this->update();
    }

    public function available(){
        $this->available=true;
        $this->update();
    }

    public function setCookie(){
        $this->cookie=true;
        $this->update();
    }

    public function unsetCookie(){
        $this->cookie=false;
        $this->update();
    }

    protected $fillable = ['broj', 'opis','cookie'];
}
