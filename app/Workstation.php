<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workstation extends Model
{
    public function NOTavailable(){
        $this->isAvailable=false;
        $this->update();
    }

    public function available(){
        $this->isAvailable=true;
        $this->update();
    }

    public function setCookie(){
        $this->isSet=true;
        $this->update();
    }

    public function unsetCookie(){
        $this->isSet=false;
        $this->update();
    }

    protected $fillable = ['number', 'description','isSet'];
}
