<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Radnik extends User
{

    public function NOTavailable(){
        $this->available=false;
        $this->update();
    }

    public function available(){
        $this->available=true;
        $this->update();
    }




    protected $fillable = ['ime', 'prezime','brojRadnika','rfid'];
}
