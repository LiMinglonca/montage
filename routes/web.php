<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/


Route::post('/odjava','TestController@odjava')->name('odjava');
Route::get('/prijava','TestController@prijava')->name('prijava');

/*
|--------------------------------------------------------------------------
| Order Routes
|--------------------------------------------------------------------------
|
*/
Route::resource('order','OrderController'); //CRUD Route
Route::post('/order/import','OrderController@importCsvData')->name('import.orders');//Importanje csv datoteke

/*--------------------------------------------------------------------------*/

/*
|--------------------------------------------------------------------------
| Operation Routes
|--------------------------------------------------------------------------
|
*/
Route::resource('operation','OperationController'); //CRUD Route
Route::get('operation/new/video/{id}','OperationController@addVideoForm')->name('new.video.form');
Route::post('operation/new/video/save','OperationController@storeVideo')->name('store.new.video');

/*--------------------------------------------------------------------------*/



/*
|--------------------------------------------------------------------------
| Video Routes
|--------------------------------------------------------------------------
|
*/
Route::get('operation/new/video/{id}','OperationController@addVideoForm')->name('new.video.form');
Route::post('operation/new/video/save','OperationController@storeVideo')->name('store.new.video');

Route::put('operation/video/update','OperationController@updateVideo')->name('video.update');
Route::delete('operation/delete/vi/deo/{id}','OperationController@deleteVideo')->name('video.destroy');
/*--------------------------------------------------------------------------*/


/*
|--------------------------------------------------------------------------
| Document Routes
|--------------------------------------------------------------------------
|
*/
Route::post('operation/doc/store','OperationController@storeDoc')->name('doc.store');
Route::delete('operation/doc/delete/{id}','OperationController@deleteDoc')->name('doc.destroy');

/*--------------------------------------------------------------------------*/

/*
|--------------------------------------------------------------------------
| Workstation Routes
|--------------------------------------------------------------------------
|
*/
Route::resource('radnaStanica', 'RadnaStanicaController');
Route::get('postaviRadnuStanicu/set','RadnaStanicaController@setCookie')->name('setRS');
Route::get('postaviRadnuStanicu/remove/{id}','RadnaStanicaController@removeRS')->name('removeRS'); //removeRS
Route::get('postaviRadnuStanicu/get','RadnaStanicaController@getCookie')->name('getRS');
Route::get('radnaStanica/import/data','RadnaStanicaController@ShowImportForm')->name('import.radnaStanica.form');
Route::post('radnaStanica/import/data/csvData','RadnaStanicaController@ImportCsvData')->name('importRadnaStanica.csv');


/*--------------------------------------------------------------------------*/

/*
|--------------------------------------------------------------------------
| Users Routes
|--------------------------------------------------------------------------
|
*/
Route::resource('radnici','RadnikController');
Route::get('radnici/import/data','RadnikController@ShowImportForm')->name('import.form');
Route::post('radnici/import/data/csvData','RadnikController@ImportCsvData')->name('import.csv');
Route::get('postavi/radnika','RadnikController@postavi')->name('setAdmin');
Route::get('izbrisi/radnika','RadnikController@izbrisi')->name('delAdmin');

/*--------------------------------------------------------------------------*/

/*
|--------------------------------------------------------------------------
| User view Routes
|--------------------------------------------------------------------------
|
*/
Route::resource('radnikView', 'RadnikViewController');
Route::resource('time','StatistikaController');
Route::post('/reset','StatistikaController@reset')->name('time.reset');
Route::get('radnik/operacija', 'RadnikViewController@showOperation')->name('operation.get');
Route::get('/statistika/order/','StatistikaController@dowload')->name('statistika.dowload');
/*--------------------------------------------------------------------------*/


/*
|--------------------------------------------------------------------------
| Gantrogram view Routes
|--------------------------------------------------------------------------
|
*/
Route::get('/gantogram','GantogramController@index')->name('gantogram.index');
/*--------------------------------------------------------------------------*/

/*
|--------------------------------------------------------------------------
| Statistka  Routes
|--------------------------------------------------------------------------
|
*/
Route::resource('/statistka','StatistikaController');
/*--------------------------------------------------------------------------*/
