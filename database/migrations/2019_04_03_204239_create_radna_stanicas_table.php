<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRadnaStanicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('radna_stanicas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('broj');
            $table->string('opis');
            $table->boolean('available')->default(true);
            $table->integer('nalog_id')->nullable();
            $table->boolean('cookie')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('radna_stanicas');
    }
}
