<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_number');
            $table->boolean('status')->default(false);//Aktivno,završeno, FALSE TRUE
            $table->string('planned_start');//planirani počeka
            $table->string('real_start')->nullable();//stvarni početak
            $table->string('planned_end');//planirani kraj
            $table->string('real_end')->nullable();//stvarni kraj
            $table->string('priority');//od 1-9, gdje je 1 najveći prioritet
            $table->string('adress')->nullable();



            $table->integer('operation_number');
            $table->integer('workstation_number');
            $table->integer('worker_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
