<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatistikasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statistikas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('order_id');
            $table->string('order'); //order number
            $table->string('opracija');
            $table->string('worker')->nullable();
            $table->string('vrijemeRada')->nullable();

            $table->string('planned_start');//planirani počeka
            $table->string('real_start')->nullable();//stvarni početak
            $table->string('planned_end');//planirani kraj
            $table->string('real_end')->nullable();//stvarni kraj




            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statistikas');
    }
}
