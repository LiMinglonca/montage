<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('radniks')->insert([
            'ime' => 'dragan',
            'prezime' => 'loncar',
            'brojRadnika' => '1',
            'rfid'=>'123',
            'isAdmin'=> true
        ]);
    }

}
