@extends('backend.layout.layout')

@section('content')
<div class="col-10 col-s-10 menu background">
        <div class="row">
            <div class="col-h3">
                <h2>Nova Radna Stanica</h2>
            </div>
        </div>
        <div class="row">
            <form id="form" action="{{route('radnaStanica.store')}}" method="POST"  enctype="multipart/form-data">
                @csrf


                <div class="row">
                  <div class="col-25">
                    <label for="fname">Broj</label>
                  </div>
                  <div class="col-8">
                    <input type="text" id="broj" name="broj">
                    <small class="small">
                            @if($errors->has('broj'))
                            <div class="alert alert-success">
                              {{$errors->first('broj')}}
                            </div>
                            @endif

                    </small>
                  </div>
                </div>
                <div class="row">
                        <div class="col-25">
                          <label for="subject">Opis Radne Stanice</label>
                        </div>
                        <div class="col-75">
                          <textarea id="opis" name="opis" style="height:200px"></textarea>
                          <small class="small">
                                  @if($errors->has('opis'))
                                  <div class="alert alert-success">
                                    {{$errors->first('opis')}}
                                  </div>
                                  @endif
                          </small>
                      </div>
                      </div>


                      </div>
                      <div class="row">
                            <div class="col-12">
                          <input class="btn bs" type="submit" value="Spremi">
                            </div>
                        </div>
            </form>
        </div>
</div>
@endsection
