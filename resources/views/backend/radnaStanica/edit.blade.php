@extends('backend.layout.layout')

@section('content')
<div class="col-10 col-s-10 menu background">
        <div class="row">
            <div class="col-h3">
                <h2>Radna Stanica: <br>{{$radnaStanica->broj}}</h2>
                @if ($radnaStanica->cookie)
                <h1>Postavljena</h1>
                @endif
            </div>
        </div>
        <div class="row">
            <form id="form" action="{{route('radnaStanica.update',$radnaStanica->id)}}" method="POST"  enctype="multipart/form-data">
                @csrf
                @method('PUT')

                <div class="row">
                  <div class="col-25">
                    <label for="fname">Broj</label>
                  </div>
                  <div class="col-8">
                  <input type="text" id="broj" name="broj" value="{{$radnaStanica->broj}}">
                    <small class="small">
                            @if($errors->has('broj'))
                            <div class="error">
                              {{$errors->first('broj')}}
                            </div>
                            @endif

                    </small>
                  </div>
                </div>
                <div class="row">
                        <div class="col-25">
                          <label >Opis radne stanice</label>
                        </div>
                        <div class="col-75">
                        <textarea id="opis" name="opis"  style="height:200px">{{$radnaStanica->opis}}</textarea>
                          <small class="small">
                                  @if($errors->has('opis'))
                                  <div class="error">
                                    {{$errors->first('opis')}}
                                  </div>
                                  @endif
                          </small>
                      </div>
                      </div>



                      </div>
                      <div class="row">
                            <div class="col-12">
                          <input class="btn bs" type="submit" value="Spremi">
                            </div>
                        </div>
                    </form>
                    <?php $stanje=false; $broj=00; ?>

                    @foreach($radneStanice as $item)
                    @if(Cookie::get($item->broj))
                    <?php $stanje=true;$broj=$item->broj ?>
                    @break
                    @else
                    <?php $stanje=false; ?>
                    @endif
                    @endforeach


                    @if(Cookie::get($radnaStanica->broj) == null )
                    @if(!$radnaStanica->cookie)

                    @if($stanje)
                    <h3>Ovo računalo je dodjeljeno radnoj stanici {{$broj}}</h3>
                    @else
                    <form id="myForm" action="{{route('setRS')}}" method="GET"  enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                          <input class="btn bs" type="button" onclick="submitForm()"  value="Postavi ovo računalo kao radnu stanicu {{$radnaStanica->broj}}">
                            <input type="hidden" name="radnaStanica_id" value="{{$radnaStanica->id}}"/>
                            <input type="hidden" name="radnaStanica_broj" value="{{$radnaStanica->broj}}"/>
                            </div>
                        </div>
                    </form>
                    @endif



                    @endif
                    @endif

                    @if (Cookie::get($radnaStanica->broj) !== null)
                    <form id="myForm" action="{{route('removeRS', $radnaStanica->id)}}" method="GET"  enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                          <input class="btn bd" type="button" onclick="submitForm()"  value="Izbriši ovo računalo kao radnu stanicu {{$radnaStanica->broj}}">
                            </div>
                        </div>
                    </form>
                    @endif
        </div>
</div>
@endsection

@section('script')

function submitForm(){

    document.getElementById("myForm").submit();
}


@endsection
