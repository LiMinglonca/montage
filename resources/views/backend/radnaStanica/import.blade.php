@extends('backend.layout.layout')

@section('content')
<div class="col-10 col-s-12 import">

<div class="row message-row">
        <div class="col-10 col-s-12">
@if(session('error'))
<span class="error">{{session('error')}}</span>
@endif
        </div>
</div>

<div class="row">
<div class="col-12 col-s-12">
    <h4 style="text-align:left">Učitaj csv datoteku radnih stanica</h4>
<form method='post' action='{{route('importRadnaStanica.csv')}}' enctype='multipart/form-data'>
    {{ csrf_field() }}
    <input type='file' name='dat' accept=".xls, .csv"  requred >
    <input type='submit' name='submit' value='Import'>
  </form>
</div>
</div>
</div>
@endsection
