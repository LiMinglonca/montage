<!--RADNE STANICE view-->
@extends('backend.layout.layout')
@section('content')
<div class="col-10 col-s-12">
    <div class="row">
            <div class="col-3 col-s-2">
            <a href="{{route('radnaStanica.create')}}" class="btn bs navButton">Nova radna stanica</a></td>
            </div>

            <div class="col-3 col-s-2">
                <a href="{{route('import.radnaStanica.form')}}" class="btn bs navButton">Import csv datoteku</a></td>
            </div>
            <div class="col-4  col-s-3"><h1></h1></div>
            <div class="col-4  col-s-3"><h1></h1></div>
            <div class="col-4  col-s-3">
                <?php $flag=false; ?>
                    @foreach($radneStanice as $radnaStanica)
                    @if(Cookie::get($radnaStanica->broj))
                  <h3  style="border:solid; padding:1px navButton">Radna stanica<br> {{$radnaStanica->broj}}</h3>
                  <?php $flag=true; ?>
                  @else
                  @endif
                    @endforeach
                    @if($flag==false)
                    <h4  style="border:solid; padding:1px navButton">Ovo računalo nije postavljeno kao radna stanica</h4>
                    @endif
            </div>
        </div>
        <div class="row">
            @if(session()->has('sucess'))
            <span class="success">
                {{ session()->get('sucess') }}
            </span>
            @endif

        @if(session()->has('error'))
            <span class="error">
                {{ session()->get('error') }}
            </span>
        @endif
        </div>
</div>
    <div class="col-10 col-s-12">
        <table>
            <thead>
              <tr>
                <th scope="col">Broj</th>
                <th scope="col">Opis</th>
                <th scope="col">Status</th>
                <th scope="col"></th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              @foreach($radneStanice as $radnaStanica)
              <tr>
                <td>{{$radnaStanica->broj}}</td>
                <td>{{$radnaStanica->opis}}</td>

                @if($radnaStanica->cookie)
                <td style="color:red">Dodijeljena</td>
                @else
                <td><b style="color:green">Slobodna</b></td>
                @endif

                <td>
                @if(Cookie::get($radnaStanica->broj) || !$radnaStanica->cookie)
                  <form action="{{ route('radnaStanica.destroy', $radnaStanica->id)}}" method="post">
                      @csrf
                      @method('DELETE')
                      <button class="btn bd" type="submit">Izbriši</button>
                   </form>
                   @endif
                </td>
                <td>
                    @if(Cookie::get($radnaStanica->broj) || !$radnaStanica->cookie)
                    <a href="{{route('radnaStanica.edit',$radnaStanica->id)}}" class="btn bs">Uredi</a>
                    @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
  </div>
  </div>
@endsection
