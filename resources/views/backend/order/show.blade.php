@extends('backend.layout.layout')
@section('content')
<div class="row">
    <div class="col-12 col-s-12">
                <div class="col-3 col-s-2">
                    <a href="{{route('order.create')}}" class="btn bs navButton">Import csv</a></td>
                </div>

                <div class="col-8 col-s-12">
                @if(session()->has('sucess'))
                <div class="row message-row sucess">

                     {{ session()->get('sucess') }}

                </div>
                @endif

                @if(session()->has('error'))
                <div class="row message-row error">

                    {{ session()->get('error') }}

                </div>
                @endif
                </div>
    </div>
</div>
<div class="row">
    <div class="col-12 col-s-12">
        <table>
            <thead>
              <tr>
                <th scope="col">Broj Naloga</th>
                <th scope="col">Prioritet</th>
                <th scope="col">Status</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach($orders as $order)
              @if ($order->check_repeats($orders) == 1)
              <tr>
                <td>{{$order->order_number}}</td>
                <td>{{$order->priority}}</td>
                @if ($order->check_status($orders))
               <td>Završeno</td>
                @else
               <td>Nedovršen</td>
                @endif
                <td>
                  <form action="{{ route('order.destroy', $order)}}" method="post">
                      @csrf
                      @method('DELETE')
                      <button class="btn bd" type="submit">Izbriši</button>
                   </form>
                </td>

               <td><a href="{{route('order.show',$order)}}" class="btn bs">Detalji</a></td>
              </tr>
              @endif
              @endforeach
            </tbody>
          </table>
  </div>
  </div>
</div>
@endsection
