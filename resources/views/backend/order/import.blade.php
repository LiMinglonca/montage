@extends('backend.layout.layout')
@section('content')
<div class="row ">
@if(session()->has('sucess'))
<div class="row message-row">
    <div class="col-6">
            <span class="success">
                    {{ session()->get('sucess') }}
                </span>
    </div>
</div>
@endif

@if(session()->has('error'))
<div class="row message-row">
    <div class="col-3"></div>
    <div class="col-6">
            <span class="error">
                    {{ session()->get('error') }}
                </span>
    </div>
</div>
@endif
</div>

<div class="row">
<div class="col-12 col-s-12 doccolor">
<form action="{{route('import.orders')}}" method="POST" enctype='multipart/form-data'>
    @csrf
<input type="file" name="orders" accept=".xls, .csv"  requred>
<input type="submit" value="Import">
</form>
</div>
</div>
@endsection























