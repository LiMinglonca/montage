@extends('backend.layout.layout')
@section('content')
<div class="row" style="border: solid 2px;">
    <div class="col-12 col-s-12" style="background-color:#008CBA;">
            <h1>Nalog: {{$order_number}}</h1>


<h3>Status:
@if ($order_obj->check_status($orders))
<h4 style="color:greeen;">Završeno</h4>
@else
<span style="color:red;">Nedovršen</span>
@endif
</h3>

</div>

    <div class="col-12 col-s-12">
        <table>
            <thead style="border: solid 1px;">
              <tr>
                <th scope="col">Operacija</th>
                <th scope="col">Radna stanica</th>
                <th scope="col">Status</th>
                <th scope="col">Pl. početak</th>
                <th scope="col">pl. kraj</th>
                <th scope="col">Vrijeme rada</th>
                <th scope="col">Radnik</th>
                <th scope="col">Početak</th>
                <th scope="col">Kraj</th>
              </tr>
            </thead>
            <tbody>
              @foreach($orders as $order)
              <tr>
                <td>{{$order->operation_number}}</td>
                <td>{{$order->workstation_number}}</td>
                @if($order->status ==True)
                <td>Završeno</td>
                @else
                <td>Nedovršen</td>
                @endif
              <td><?php echo date('H:i d-m-Y',$order->planned_start); ?></td>
              <td><?php echo date('H:i d-m-Y',$order->planned_end); ?></td>


              @foreach ($statistika as $sorder)
              @if ($order->id == $sorder->order_id)
                <td>{{$sorder->vrijemeRada}}</td>
                <td>{{$sorder->worker}}</td>
                <td><?php if(!empty($sorder->real_start)){echo date('H:i:s d-m-Y',$sorder->real_start);}?></td>
                <td><?php if(!empty($sorder->real_end)){echo date('H:i:s d-m-Y',$sorder->real_end);}?></td>
              @endif
              @endforeach


            </tr>
              @endforeach
            </tbody>
          </table>

  </div>
  </div>
  <div class="row">
      <div class="col-4 col-s-4 ">
        <a class="btn bs" href="{{ url()->previous() }}">Nazad</a>
    </div>
    <div class="col-4 col-s-4 ">

        </div>
        <div class="col-4 col-s-4 ">
                <form action="{{route('statistika.dowload')}}" method="GET">
                        <input type="hidden" name="order" value="{{$order->order_number}}">
                        <input id="bdowload" class="btn " type="submit" name="dowload" value="Dowload">
                </form>
            </div>
    </div>

@endsection

@section('script')

$( document ).ready(function() {
    var status ={!! json_encode($order->check_status($orders)) !!};

    console.log(status);

    if(status){
        $("#bdowload").show();
    }
    else{
        $("#bdowload").hide();
    }
});

@endsection
