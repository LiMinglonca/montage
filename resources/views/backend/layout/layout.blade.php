<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="{{ asset('/css/style.css') }}" rel="stylesheet">
<link href="{{ secure_asset('/css/style.css') }}" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>

<div class="header">
    <div class="row">
        <div class="col-4"><h1>LeanFactory <small style="font-size:8pt">AdminPage</small></h1></div>
        <div class="col-1"></div>
        <div class="col-6"></div>
        <div class="col-1">
        <form action="{{route('odjava')}}" method="POST">
            @csrf
                <input type="hidden" name="logout" value="true">
                <h3>Radnik: {{Session::get('radnik')->brojRadnika}}</h3>
                <button type="submit" style="background-color:red;"> <h3 style="display:block">Odjava</h3>
                </button>
            </form>

        </div>
        </div>
</div>

<div class="row">
  <div class="col-3 col-s-12 menu">
    <ul>
     <a  href="{{route('order.index')}}"> <li class="menu-height">Nalozi</li></a>
     <a  href="{{route('operation.index')}}"> <li class="menu-height">Operacije</li></a>
     <a  href="{{route('radnaStanica.index')}}"> <li class="menu-height">Radne stanice</li></a>
     <a href="{{route('radnici.index')}}"> <li class="menu-height">Radnici</li></a>
     <a href="{{route('gantogram.index')}}"> <li class="menu-height">Raspored</li></a>
    <a href="{{route('statistka.index')}}"> <li class="menu-height">Izvoz podataka</li></a>
    </ul>
  </div>
  <div class="col-9 col-s-12">
  @yield('content')
  </div>
</div>
<script>
@yield('script')
</script>
</body>
</html>
