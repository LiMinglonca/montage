<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="{{ asset('/css/style.css') }}" rel="stylesheet">
<link href="{{ secure_asset('/css/style.css') }}" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>

<div class="header">
  <h1>{{ config('app.name') }}</h1>
</div>

<div class="row">
  <div class="col-2 col-s-12 menu">
    <ul>
     <a href="{{route('nalog.index')}}"> <li>Nalozi</li></a>
     <a href="{{route('operacije.index')}}"> <li>Operacije</li></a>
     <a href="{{route('radnici.index')}}"> <li>Radnici</li></a>
     <a href="{{route('radnaStanica.index')}}"> <li>Radne stanice</li></a>
     <a href="{{route('time.index')}}"> <li>Statisitka</li></a>
    </ul>
  </div>
  @yield('content')

</div>
<script>
@yield('script')
</script>
</body>
</html>
