@extends('backend.layout.layout')
@section('content')
<div class="col-4 col-s-12">
   <div class="row"> <a href="{{route('operation.create')}}" class="btn bs">Nova operacija</a></td></div>

   @if(session()->has('error'))
<div class="row message-row">
    <div class="col-6"></div>
    <div class="col-6">
            <span class="error">
                    {{ session()->get('error') }}
                </span>
    </div>
</div>
@endif

@if(session()->has('sucess'))
<div class="row message-row">
    <div class="col-3"></div>
    <div class="col-6">
            <span class="sucess">
                    {{ session()->get('sucess') }}
                </span>
    </div>
</div>
@endif
</div>
    <div class="col-12 col-s-12">
        <table>
            <thead style="min-width:10%;">
              <tr>
                <th scope="col">Broj Operacije</th>
                <th scope="col">Ime Operacije</th>
                <th scope="col">Opis</th>
                <th scope="col"></th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              @foreach($operations as $operation)
              <tr>
                <td >{{$operation->operation_number}}</td>
                <td>{{$operation->title}}</td>
                <td >{{$operation->description}}</td>
                <td>
                  <form action="{{ route('operation.destroy', $operation)}}" method="post">
                      @csrf
                      @method('DELETE')
                      <button class="btn bd" type="submit">Izbriši</button>
                   </form>
                </td>
                <td><a href="{{route('operation.edit',$operation->id)}}" class="btn bs">Detalji</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
  </div>

@endsection

@section('script')
jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});
@endsection
