@extends('backend.layout.layout')
@section('content')
<div class="col-12 col-s-10 menu background"><!--COLUMNA FORME-->

    <!--RED NASLOVA-->
    <div class="row">
        <div class="col-h3">
        <h2>Izmjena Operacije:<br> <b>{{$operation->operation_number}}</b></h2>
        </div>
    </div>
<!--KRAJ REDA NASLOVA-->


<!--RED FORME-->
    <div class="row">
        <form id="form" action="{{route('operation.update', $operation)}}" method="POST"  enctype="multipart/form-data">
            @csrf
            @method('PUT')
<!--RED ZA NAZIV OPERACIJE-->
            <div class="row">
              <div class="col-25">
                <label>Naziv operacije</label>
              </div>
              <div class="col-75">
              <input type="text"name="title" value="{{$operation->title}}">
              </div>
            </div>

            <div class="row">
                <div class="col-25">
                  <label>Broj operacije</label>
                </div>
                <div class="col-75">
                <input type="text"  name="operation_number" value="{{$operation->operation_number}}">
                </div>
              </div>
<!--KRAJ REDA ZA NAZIV OPERACIJE-->
<!--RED ZA OPIS OPERACIJE-->
            <div class="row">
              <div class="col-25">
                <label class="label">Opis operacije</label>
              </div>
              <div class="col-75">
              <textarea name="description" placeholder="Opis se odonosi na video i doc" style="height:200px">{{$operation->description}}</textarea>
            </div>
            </div>
            <div class="row">
            <div class="col-4">
                <div class="row">
                <input class="btn bs" type="submit" value="Spremi"><!--SPREMI BUTTON TJ ROW-->
            </div>
            </div>
                <div class="col-4">
                        <div class="row">
                       <a href="{{route('new.video.form', $operation->id)}}" class="btn bs">Dodaj Video</a>
                        </div>
                </div>
            </div>
        </div>
        </form>
<!--KRAJ REDA ZA OPIS OPERACIJE-->

<!--RED ZA VIDEO I docE-->
@foreach($videos as $video)
<div class="row"><hr class="hrborder"></div>

<div class="row">



    <!--COLUMNA ZA VIDEO-->
<div class="col-6"  >
            <!--RED ZA VIDEO PLAYER-->
            <div class="row">
                <video controls width="100%" height="100%"  muted>
                <source src={{asset('Videos\\'.$video->name)}} type="video/mp4">
                  </video>
            </div>
            <!--KRAJ REDA ZA VIDEO PLAYER-->
            <div class="row">
            <div class="col-4 col-s-2">
            <button type="button" idVideo="{{$video->id}}" class="btn bd video" videoToken="{{ csrf_token() }}" >Izbriši Video</button>
            </div>

            <div class="col-4 col-s-3">
            <form enctype="multipart/form-data" action="{{route('video.update', $video->id)}}" method="POST"   id="colVideo-{{$video->id}}" onchange="getid(this.id)">
                @csrf
                @method('PUT')
                    <label  class="btn bs">
                    <input  name="video"  accept="video/*"  id="{{$video->id}}" type="file" style="display:none" class=" btn bs video_attachment addbut" />
                           Izmjeni Video
                    <input type="hidden" name="operation_id" value="{{$operation->id}}"/>
                    <input type="hidden" name="video_id" value="{{$video->id}}"/>
                    </label>
                    <label class="countVideo"></label>
                <input type="submit" class="btn bs save-video" id="savevideo-{{$video->id}}" value="Spremi Promjene Videa">
            </form>
            </div>
        </div>
    </div>
            <!--KRAJ COLUMNE ZA VIDEO-->


            <!--COLUMNA ZA POPIS DOKUMENATA-->
            <div class="col-5">
                <div class="row">
                @foreach ($docs as $doc)
                <table>
                        <tbody>
                @foreach($doc as $item)
                @for($i=0;$i<count($item);$i++)
                @if($item[$i]->video_id == $video->id)
                          <tr>
                            <td>{{$item[$i]->user_name_of_doc}}</td>
                            <td></td>
                            <td>
                            <button type="button" iddoc="{{$item[$i]->id}}" class="btn bd doc" docToken="{{ csrf_token() }}" >Izbriši</button>
                            </td>
                          </tr>
                          @endif
                @endfor
                @endforeach
            </tbody>
        </table>
                @endforeach
            </div>
                <div class="row">
                    <br>
                    <div class="col-3"></div>
                    <div class="col-6">
                <form onchange="dgetid(this.id)" id="docID{{$video->id}}" action="{{route('doc.store')}}" method="POST"  enctype="multipart/form-data">
                        @csrf
                            <label  class="btn bs">
                                <input style="display: none"  type="file"  id="doc" name="doc[]" accept=".pdf,.doc,.docx" multiple >
                                Dodaj Dokument
                            </label>
                            <label id="countdoc{{$video->id}}"></label>
                        <input type="submit" class="btn bs" id="save-doc{{$video->id}}" value="Spremi Promjene Dokumenata" />
                        <input type="hidden" name="operacija" value="{{$operation->id}}" />
                        <input type="hidden" name="video" value="{{$video->id}}" />


                    </form>
                    </div>
                    </div>
                </div>
            <!--KRAJ KOLUMNE ZA POPIS docE-->
            </div>
        <!--KRAJ REDA ZA VIDEO I OPERACIJE-->

<!--KRAJ REDA FORME-->
@endforeach
<!--KRAJ REDA ZA VIDEO I OPERACIJE-->

</form>
<!--KRAJ FORME-->
</div>
</div>
<!--KRAJ COLUMNE ZA FORMU--> --}}
@endsection


 @section('script')

$(document).ready(function(){
@foreach ($videos as $video)
var filesV = $("[name='video']").prop("files");
   if(filesV.length <= 0){
   $("#savevideo-{{$video->id}}").hide();
    }

    var filesD = $("[name='doc[]']").prop("files");
    if(filesD.length==0){
        $("#save-doc{{$video->id}}").hide();
   }


@endforeach

 });


function getid(id){
    var filesV = $("[name='video']").prop("files");
    var numb = id.match(/\d/g);
    numb = numb.join("");

    $("#savevideo-"+numb).show();
    $('.countVideo').append('Novi video učitan');
}

function dgetid(id){


var realID = id.replace('docID','');
var numb = id.match(/\d/g);
numb = numb.join("");

var filesD = $("[name='doc[]']").prop("files");

         $('#countdoc'+realID).empty();
       $('#countdoc'+realID).append('Dodani dokument(i)');
     $("#save-doc"+numb).show();

}


    function getFile(){
        document.getElementById("doc").click();
   }


   //BUTTON ZA BRISANJE
   $(document).on('click', ".doc", function() {
    var iddoc=$(this).attr('iddoc');
    var docToken = $(this).attr("docToken");

    var url = '{{route("doc.destroy","id")}}';
    url = url.replace('id', iddoc);

    $.ajax({
        type: 'POST',
        url: url,
        data: {
            _method:"DELETE",
            'id' : iddoc
        },
        headers: { 'X-CSRF-TOKEN' : docToken },
        success: function(response) {
            console.log("work");
            location.reload();
        }

   });
});

$(document).on('click', ".video", function() {
    console.log("video ajax");
    var idVideo=$(this).attr('idVIdeo');
    var videoToken = $(this).attr("videoToken");

    var url = '{{route("video.destroy","id")}}';
    url = url.replace('id', idVideo);
    console.log(idVideo);

    $.ajax({
        type: 'POST',
        url: url,
        data: {
            _method:"DELETE",
            'id' : idVideo
        },
       headers: { 'X-CSRF-TOKEN' : videoToken },
        success: function(response) {
            console.log("work video ajax");
            location.reload();
        }

   });

});
@endsection
