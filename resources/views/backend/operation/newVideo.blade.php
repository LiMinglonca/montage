@extends('backend.layout.layout')
@section('content')
<div class="col-10 col-s-10 menu background">
        <div class="row">
            <div class="col-h3">
                <h2>Novi video za Operaciju {{$operation->operation_number}}</h2>
            </div>
        </div>
        <div class="row">
            <form id="form" action="{{route('store.new.video')}}" method="POST"  enctype="multipart/form-data">
                @csrf
    <div class="row b2"id="dynamic_field">
                <div class="row">
                        <div class="row">
                                <div class="col-25">
                                  <label>Opis Videa</label>
                                </div>
                                <div class="col-75">
                                  <textarea disabled name="description" style="height:100px" required>{{$operation->description}}</textarea>
                              </div>
                              </div>

                <div class="col-3"></div>



                <div class="col-25">


                <div class="row">
                        <label class="custom-file-upload">
                                <i class="fa fa-cloud-upload"></i>Dodaj Video
                              </label>
                </div>
                <div class="row">
                <input type="file" id="video0" name="video0"  accept="video/*" >
                </div>
                </div>



                <div class="col-25">


                    <div class="row">
                            <label class="custom-file-upload">
                                    <i class="fa fa-cloud-upload"></i>Dodaj Dokument(e)
                                  </label>
                    </div>
                    <div class="row">
                                  <input  type="file"  id="doc0" name="doc0[]" accept=".pdf,.doc,.docx" multiple >
                    </div>
                    </div>
                    <div class="col-2">
                    <input class="btn bs" id="add"  type="button" value="+">
                    </div>

                </div>
                <!--KRAJ PRVI RED-->
<!--Drugi red ..-->

    </div>
            </div>
        <input type="hidden" name='operation_id' value="{{$operation->id}}">
            <div class="row">
                    <div class="col-2">
                  <input class="btn bs" type="submit" value="Spremi">
                    </div>
                </div>

</form>

</div>
@endsection
@section('script')
$(document).ready(function(){
    var postURL = "<?php echo url('addmore'); ?>";
    var i=0;


    $('#add').click(function(){
         i++;

         var appendME='<div class="row b2" id="row'+i+'">'+
         '<div class="col-3"></div>'+
         '<div class="col-25">'+
         '<div class="row">'+
        '<label for="file">Video</label>'+
         '</div>'+
         '<div class="row">'+
         '<input type="file" id="video'+i+'" name="video'+i+'"  accept="video/*" >'+
         '</div>'+
         '</div>'+
         '<div class="col-25">'+
             '<div class="row">'+
            '<label for="fname">Dokument</label>'+
             '</div>'+
             '<div class="row">'+
                     '<input  type="file"  id="doc'+i+'" name="doc'+i+'[]" accept=".pdf,.doc,.docx" multiple >'+
                    '</div>'+
                '</div>'+
                '<div class="col-2">'+
                '<input class="btn bd btn_remove" id="'+i+'"  type="button" value="-">'+
                '</div>'+

            '</div>';

         $('#dynamic_field').append(appendME);
 });


    $(document).on('click', '.btn_remove', function(){
         var button_id = $(this).attr("id");
         $('#row'+button_id+'').remove();
         //alert("kliknut"+button_id+'');
    });

  });


@endsection
