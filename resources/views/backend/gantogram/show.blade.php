@extends('backend.layout.layout')
@section('content')
<style>

        #box{

            background-color: #008CBA;
            display: inline-flex;
            min-width: 60%;
            min-height: 100px;
            margin : 5px;
            align: center;
            float:inherit;

        }

       #box-warning{
        background-color: red;
        display: inline-flex;
            min-width: 60%;
            min-height: 100px;
            margin : 10px;
            align: center;
            float:inherit;
        }

     h4{
           font-size: 10pt;

           min-width:5%;


        }

    #header{
        background-color: #008CBA;
        min-width: 60%;

        min-height: 50px;

    }
#border{
    border: solid 2px;
    margin: 2%;
}


</style>
@foreach ($RS as $radnaStanica)
<div class="row" id="border">
<div class="row" style="border-bottom:solid 2px;">
    <div class="col-12" id=header>
        <h1 id="header">Radna stanica : {{$radnaStanica->broj}}</h1>
    </div>
</div>

@foreach ($data as $item)
<div class="row">
@if($radnaStanica->broj == $item->workstation_number)
@if(!$item->status)
<div class="col-10 col-s-10" id="<?php
$currentTime = time();

$real_end = $item->planned_end;
#echo date('H:i:s d-m-Y',$currentTime)."<br>";
#echo date('H:i:s d-m-Y',$real_end);






if($currentTime > $real_end){
    echo "box-warning";
}
else{
    echo "box";
}

?>">
<div class="col-3 col-s-4"><h4>Broj naloga : {{$item->order_number}}</h4></div>
<div class="col-3 col-s-6"><h4>Broj operacije : {{$item->operation_number}}</h4></div>
<div class="col-3 col-s-6"><h4>Pl. početak : <?php echo date('H:i d-m-Y',$item->planned_start); ?></h4></div>
<div class="col-3 col-s-6"><h4>Pl. kraj : <?php echo date('H:i d-m-Y',$item->planned_end); ?></h4></div>
<div class="col-3 col-s-6"><h4><?php if($item->real_start){echo "Trenutno se izvršava";}?></h4></div>

</div>
@endif
@endif
</div>
@endforeach
</div>
@endforeach


@endsection


