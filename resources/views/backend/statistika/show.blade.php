@extends('backend.layout.layout')
@section('content')
<div class="row">
    <div class="col-12 col-s-12">
        <table>
            <thead>
              <tr>
                <th scope="col">Broj Naloga</th>
                <th></th>
                <th></th>

              </tr>
            </thead>
            <tbody>
              @foreach($orders as $order)
              @if ($order->check_repeats($orders) == 1)
              @if ($order->check_status($orders))
              <tr>
                <td>{{$order->order}}</td>
                <td></td>

               <td>
               <!-- <a href="{{route('statistika.dowload',$order->order)}}" class="btn bs">Dowload</a>-->
                <form action="{{route('statistika.dowload')}}" method="GET">
                <input type="hidden" name="order" value="{{$order->order}}">
                <input id="bdowload"class="btn bg" type="submit" name="dowload" value="Dowload">
                </form>

               </td>
              </tr>
              @endif
              @endif
              @endforeach
            </tbody>
          </table>
  </div>
  </div>
</div>
@endsection
