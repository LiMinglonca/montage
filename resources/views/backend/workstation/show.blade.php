<!--RADNE STANICE view-->
@extends('backend.operacije.layout')
@section('content')
<div class="col-10 col-s-12">
    <div class="row">
            <div class="col-3 col-s-2">
            <a href="{{route('workstation.create')}}" class="btn bs navButton">Nova radna stanica</a></td>
            </div>

            <div class="col-3 col-s-2">
                {{-- <a href="{{route('import.workstation.form')}}" class="btn bs navButton">Import csv datoteku</a></td> --}}
            </div>
            <div class="col-3 col-s-6"></div>

            <div class="col-2  col-s-2">
                <?php $flag=false; ?>
                    @foreach($workstations as $workstation)
                    @if(Cookie::get($workstation->number))
                  <h3  style="border:solid; padding:1px navButton">Radna stanica<br> {{$workstation->number}}</h3>
                  <?php $flag=true; ?>
                  @else

                  @endif
                    @endforeach
                    @if($flag==false)
                    <h4  style="padding:1px navButton">Ovo računalo nije postavljeno kao radna stanica</h4>
                    @endif
            </div>
        </div>
        <div class="row">
            @if(session()->has('sucess'))
            <span class="success">
                {{ session()->get('sucess') }}
            </span>
            @endif

        @if(session()->has('error'))
            <span class="error">
                {{ session()->get('error') }}
            </span>
        @endif
        </div>
</div>
    <div class="col-10 col-s-12">
        <table>
            <thead>
              <tr>
                <th scope="col">number</th>
                <th scope="col">Opis</th>
                <th scope="col">Status</th>
                <th scope="col"></th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              @foreach($workstations as $workstation)
              <tr>
                <td>{{$workstation->number}}</td>
                <td>{{$workstation->description}}</td>

                @if($workstation->cookie)
                <td style="color:red">Dodijeljena</td>
                @else
                <td><b style="color:green">Slobodna</b></td>
                @endif

                <td>
                @if(Cookie::get($workstation->number) || !$workstation->cookie)
                  <form action="{{ route('workstation.destroy', $workstation->id)}}" method="post">
                      @csrf
                      @method('DELETE')
                      <button class="btn bd" type="submit">Izbriši</button>
                   </form>
                   @endif
                </td>
                <td>
                    @if(Cookie::get($workstation->number) || !$workstation->cookie)
                    <a href="{{route('workstation.edit',$workstation->id)}}" class="btn bs">Uredi</a>
                    @endif
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
  </div>
  </div>
@endsection
