@extends('backend.layout.layout')

@section('content')
<div class="col-10 col-s-10 menu background">
        <div class="row">
            <div class="col-h3">
                @if(isset($radnik))
            <h2>Izmjena radnika:<br> {{$radnik->ime}}  {{$radnik->prezime}}  {{$radnik->brojRadnika}}</h2>
            @endif
            </div>
        </div>
        <div class="row">
            <form id="form" action="{{route('radnici.update', $radnik->id)}}" method="POST"  enctype="multipart/form-data">
                @csrf
                @method('PUT')

                <div class="row">
                  <div class="col-25">
                    <label for="fname">Ime</label>
                  </div>
                  <div class="col-8">
                  <input type="text" id="ime" name="ime" value="{{$radnik->ime}}"/>
                    <small class="small">
                            @if($errors->has('ime'))
                            <div class="alert alert-success">
                              {{$errors->first('ime')}}
                            </div>
                            @endif

                    </small>
                  </div>
                </div>


                <div class="row">
                  <div class="col-25">
                    <label for="subject">Prezime</label>
                  </div>
                  <div class="col-8">
                        <input type="text" id="prezime" name="prezime" value="{{$radnik->prezime}}">
                    <small class="small">
                            @if($errors->has('imeOperacije'))
                            <div class="alert alert-success">
                              {{$errors->first('opisOperacije')}}
                            </div>
                            @endif
                    </small>
                </div>
                </div>
                <div class="row">
                        <div class="col-25">
                          <label for="subject">Broj Radnika</label>
                        </div>
                        <div class="col-8">
                              <input type="text" id="ID" name="brojRadnika" value="{{$radnik->brojRadnika}}">
                          <small class="small">
                                  @if($errors->has('imeOperacije'))
                                  <div class="alert alert-success">
                                    {{$errors->first('opisOperacije')}}
                                  </div>
                                  @endif
                          </small>
                      </div>
                      </div>
                      <div class="row">
                            <div class="col-12">
                          <input class="btn bs" type="submit" value="Spremi">
                            </div>
                        </div>
            </form>
            <div class="row">
                    <div class="col-12">
                    <form id="postavi" action="{{route('setAdmin')}}" method="GET">
                        @csrf
                                 <input type="hidden" name="id" value="{{$radnik->id}}">
                                 <input class="btn bs" type="submit" value="Postavi kao poslovođu">
                    </form>

                    </div>
                </div>

                <div class="row">
                        <div class="col-12">
                        <form id="izbrisi" action="{{route('delAdmin')}}" method="GET">
                            @csrf
                                     <input type="hidden" name="id" value="{{$radnik->id}}">
                                     <input class="btn bd" type="submit" value="Postavi kao Montazera">
                        </form>

                        </div>
                    </div>
        </div>
</div>
@endsection

@section('script')
var status = <?php echo $radnik->isAdmin; ?>;
$( ".target" ).hide();
$( ".target" ).hide();
console.log(status);

$( document ).ready(function() {
    $( "#postavi" ).hide();
    $( "#izbrisi" ).hide();

    if(status == 1){
        $( "#izbrisi" ).show();
    }
    else{
        $( "#izbrisi" ).hide();
        $( "#postavi" ).show();
    }
});


@endsection
