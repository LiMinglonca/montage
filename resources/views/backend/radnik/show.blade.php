@extends('backend.layout.layout')
@section('content')

<div class="col-10 col-s-12">
        <div class="row">
                <div class="col-3 col-s-2">
                <a href="{{route('radnici.create')}}" class="btn bs navButton">Dodaj radnika</a></td>
                </div>

                <div class="col-3 col-s-2">
                    <a href="{{route('import.form')}}" class="btn bs navButton">Import csv</a></td>
                </div>
                <div class="col-3 col-s-6"></div>
            </div>
            <div class="row">
                @if(session()->has('sucess'))
                <span class="success">
                    {{ session()->get('sucess') }}
                </span>
                @endif

            @if(session()->has('error'))
                <span class="error">
                    {{ session()->get('error') }}
                </span>
            @endif
            </div>
    </div>





<div class="col-6 col-s-12">

</div>
    <div class="col-10 col-s-12">
        <table>
            <thead>
              <tr>
                <th scope="col">Ime</th>
                <th scope="col">Prezime</th>
                <th scope="col">ID</th>
                <th scope="col">Status</th>
                <th scope="col"></th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              @foreach($radnici as $radnik)
              <tr>
                <td data-label="Ime">{{$radnik->ime}}</td>
                <td data-label="Prezime">{{$radnik->prezime}}</td>
                <td data-label="ID">{{$radnik->brojRadnika}}</td>
                <td><?php
                if($radnik->isAdmin){
                    echo "Poslovođa";
                }
                else{
                    echo "Montažer";
                }
                ?></td>
                <td>
                  <form action="{{ route('radnici.destroy', $radnik->id)}}" method="post">
                      @csrf
                      @method('DELETE')
                      <button class="btn bd" type="submit">Izbriši</button>
                   </form>
                </td>
                <td><a href="{{route('radnici.edit',$radnik->id)}}" class="btn bs">Uredi</a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
  </div>
  </div>
@endsection
