@extends('backend.layout.layout')

@section('content')
<div class="col-10 col-s-10 menu background">
        <div class="row">
            <div class="col-h3">
                <h2>Novi Radnik</h2>
            </div>
        </div>
        <div class="row">
            <form id="form" action="{{route('radnici.store')}}" method="POST"  enctype="multipart/form-data">
                @csrf


                <div class="row">
                  <div class="col-25">
                    <label for="fname">Ime</label>
                  </div>
                  <div class="col-8">
                    <input type="text" id="ime" name="ime">
                    <small class="small">
                            @if($errors->has('ime'))
                            <div class="alert alert-success">
                              {{$errors->first('ime')}}
                            </div>
                            @endif

                    </small>
                  </div>
                </div>


                <div class="row">
                  <div class="col-25">
                    <label for="subject">Prezime</label>
                  </div>
                  <div class="col-8">
                        <input type="text" id="prezime" name="prezime">
                    <small class="small">
                            @if($errors->has('imeOperacije'))
                            <div class="alert alert-success">
                              {{$errors->first('opisOperacije')}}
                            </div>
                            @endif
                    </small>
                </div>
                </div>
                <div class="row">
                        <div class="col-25">
                          <label for="subject">Broj Radnika</label>
                        </div>
                        <div class="col-8">
                              <input type="text" id="ID" name="brojRadnika">
                          <small class="small">
                                  @if($errors->has('imeOperacije'))
                                  <div class="alert alert-success">
                                    {{$errors->first('opisOperacije')}}
                                  </div>
                                  @endif
                          </small>
                      </div>
                      </div>

                      <div class="row">
                        <div class="col-25">
                          <label for="subject">RFID</label>
                        </div>
                        <div class="col-8">
                              <input type="text" id="rfid" name="rfid">
                      </div>
                      </div>

                      <div class="row">
                            <div class="col-12">
                          <input class="btn bs" type="submit" value="Spremi">
                            </div>
                        </div>
            </form>
        </div>
</div>
@endsection
