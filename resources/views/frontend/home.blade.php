@extends('frontend.layout')
@section('content')
<div class="row">
<div class="col-12">

    <div class="row">



        <form action="{{route('time.store')}}" method="POST" id="myForm">

           <input type="text" value="00:00:00" id="timerLabel" name="time">
            <div class="row">
            <div class="col-2">
            <button type="button" class="btn bs" style="background-color:rgb(228, 228, 1)" onclick="zaustavi()" id="stop">Pauza</button>
            <button type="button" class="btn bs" onclick="pocni()" id="start">Nastavi</button>
            </div>
            </div>
            <div class="row">
            <div class="col-2">
            <button type="button" class="btn bd" id="submitButton" onclick="submitTime()">Završi</button>
            </div>
            </div>
            @csrf
            <input type="hidden" name="operacja" value="{{$operacija->operation_number}}">
            <input type="hidden" name="order_id" value="{{$order_id}}">
            <input type="hidden" name="worker" value="{{Session::get('radnik')->rfid}}">

        </form>
        <form action="{{route('time.reset')}}" method="POST" id="prekid">
            @csrf
                <div class="col-2">
                        <input type="hidden" name="order_id" value="{{$order_id}}">
                        <input type="hidden" name="operation" value="{{$operacija->operation_number}}">
                        <button type="button" class="btn bd" id="ff" onclick="prekid()"><b>Prekid</b></button>
                </div>
        </form>
    </div>

</div>
</div>
<div class="row">
    @foreach($videos as $video)
    <div class="row"><hr class="hrborder"></div>
    <div class="row">
        <div class="col-8">
            <div class="row">
                <video muted controls width="100%" height="50%" >
                        <source src={{asset('Videos\\'.$video->name)}} type="video/mp4">
                </video>
        </div>
    </div>
        <div class="col-4">
            <div class="row">
                    @foreach ($dokumenti as $dokument)
                    <table id="hide">
                    <tbody>
                    @foreach($dokument as $items)

                    @foreach ($items as $item)

                    @if($item->video_id == $video->id)
                    <div>
                    <tr>

                        <td>{{$item->user_name_of_doc}}</td>
                        <td><a class="btn bs" href="{{asset('Docs\\'.$item->name)}}" target="_blank">Otvori</a></td>
                        <!-- <td><button class="btn bs" id="{{asset('Docs\\'.$item->name)}}" onclick="hide(this.id)">Prikazi</button></td> -->
                        </tr>
                    </div>


                        @endif
                    @endforeach

                    @endforeach
                </tbody>
            </table>

                    @endforeach
            </div>
        </div>
    </div>

    @endforeach


</div>


@endsection

@section('script')
$('#start').hide();

function submitTime(){

    document.getElementById("myForm").submit();
}

function prekid(){

    document.getElementById("prekid").submit();
}


var status = 0; // 0:stop 1:running
pocni();
var time = 0;
var startBtn = document.getElementById("startBtn");
var timerLabel = document.getElementById('timerLabel');

function pocni(){
    status = 1;
    timer();

    $('#stop').show();
    $('#start').hide();
}




    function zaustavi(){
        status = 0;
        $('#stop').hide();
        $('#start').show();

    }


    function reset(){
        status = 0;
        time = 0;
        timerLabel.innerHTML = '00:00:00';
        startBtn.disabled = false;
    }
    function timer(){
        if (status == 1) {
            setTimeout(function() {
                time++;
                var min = Math.floor(time/100/60);
                var sec = Math.floor(time/100);
                var mSec = time % 100;
                if (min < 10) min = "0" + min;
                if (sec >= 60) sec = sec % 60;
                if (sec < 10) sec = "0" + sec;
                if (mSec < 10) mSec = "0" + mSec;
                document.getElementById('timerLabel').value = min + ":" + sec + ":" + mSec;
                timer();
            }, 10);
        }
    }
    document.onkeydown = function(event) {
        if (event) {
            if (event.keyCode == 32 || event.which == 32) {
                if(status == 1) {
                    stop();
                } else if (status == 0) {
                    start();
                }
            }
        }
    };

    function hide(id){


        var e = document.getElementById("ch");
        document.getElementById("ch").src=id;

        $( "#showPDF" ).show();
    }


@endsection
