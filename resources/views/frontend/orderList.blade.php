
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="{{ asset('/css/style.css') }}" rel="stylesheet">
<link href="{{ secure_asset('/css/style.css') }}" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>


         @if(Session::get('radnik'))

        @if(Session::get('radnik')->isAdmin == True)
        <script>window.location = "{{route('order.index')}}";</script>
        @endif

        @else
        <script>window.location = "{{route('http://localhost/client-montage/home.php')}}";</script>

        @endif

        <div class="header">
                <div class="row">
                    @if(isset(Session::get('radnik')->brojRadnika))
                <div class="col-2"><h3>Radnik:{{Session::get('radnik')->brojRadnika}}</h3></div>
                @endif
                @if(Session::get('workstation'))
                <div class="col-2"><h3 style="display:block">Radno mjesto : {{Session::get('workstation')}}</h3></div>
                @else
                <div class="col-2"><h3 style="display:block; background-color:red; border:2px">Radno mjesto : Nije postavljeno</h3></div>
                @endif
                <div class="col-7"></div>
                <div class="col-1">
                <form action="{{route('odjava')}}" method="POST">
                    @csrf
                        <input type="hidden" name="logout" value="true">
                        <button type="submit" style="background-color:red;"> <h3 style="display:block">Odjava</h3>
                        </button>
                    </form>

                </div>
                </div>
                </div>
                 <hr>
                 <div class="row message-row">
                        <div class="col-10 col-s-12">
                @if(session('Error'))
                <span class="error">{{session('Error')}}</span>
                @endif
                        </div>
                </div>
<div class="row">


@foreach ($orders as $order)
@if(Session::get('workstation') == $order->workstation_number)
@if($order->status == False)
<div class="col-3" style="min-height:2cm">

<form action="{{route('operation.get')}}" method="get">
                @csrf
                <input type="hidden" name="operation_number" value="{{$order->operation_number}}">
                <input type="hidden" name="order_number" value="{{$order->order_number}}">
                <input type="hidden" name="order_id" value="{{$order->id}}">

<button class="btn bd" type="submit"><h3>Nalog: {{$order->order_number}}</h3>  <h4>Operacija: {{$order->operation_number}}</h4> <h4><?php echo date('H:i d-m-Y', $order->planned_start);?></h4></button>
             </form>
</div>
@endif
@endif
@endforeach

</div>
<br>
</div>
<script>

</script>
</body>
</html>

