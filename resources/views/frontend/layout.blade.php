<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="{{ asset('/css/style.css') }}" rel="stylesheet">
<link href="{{ secure_asset('/css/style.css') }}" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>
        <div class="header">
                <div class="row">
                <div class="col-1"><h3>Radnik: {{Session::get('radnik')->brojRadnika}}</h3></div>
                <div class="col-1"><h3 style=" display:block">Nalog: {{Session::get('order')}}</h3></div>
                <div class="col-1"><h3 style=" display:block">Operacija: {{$operacija->operation_number}}</h3></div>
                <div class="col-1"></div>
                <div class="col-1">
                            <h3 style="display:block">Radno mjesto: {{Session::get('workstation')}}</h3>

                </div>
                </div>

                </div>
                 </div>
                 <hr>
<div class="row">

  @yield('content')

</div>
<br>
</div>
<script>
@yield('script')
</script>
</body>
</html>
